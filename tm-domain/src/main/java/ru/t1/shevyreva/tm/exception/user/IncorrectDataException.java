package ru.t1.shevyreva.tm.exception.user;

public class IncorrectDataException extends AbstractUserException {

    public IncorrectDataException() {
        super("Error! Incorrect data.");
    }

}
