package ru.t1.shevyreva.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.Session;

import java.util.List;

public interface ISessionRepository extends IAbstractUserOwnedModelRepository<Session> {

    void removeOne(@NotNull final Session session);

    @Override
    public void clearForUser(@NotNull String userId);

    @Override
    public void removeOneByIdForUser(@NotNull String userId, @NotNull String id);

    @Override
    public List<Session> findAllForUser(@NotNull String userId);

    @Override
    public Session findOneByIdForUser(@NotNull String userId, @NotNull String id);

    @Override
    public int getSizeForUser(@NotNull String userId);

    @Override
    public void clear();

    @Override
    public @Nullable List<Session> findAll();

    @Override
    public @Nullable Session findOneById(@NotNull String id);

    @Override
    public void removeOneById(@NotNull String id);

    @Override
    public int getSize();

}
