package ru.t1.shevyreva.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.model.TaskDTO;

import java.util.Comparator;
import java.util.List;

public interface ITaskDtoRepository extends IAbstractUserOwnedModelDtoRepository<TaskDTO> {

    void clearForUser(@NotNull String userId);

    void clear();

    List<TaskDTO> findAllForUser(@NotNull String userId);

    List<TaskDTO> findAll();

    List<TaskDTO> findAll(@Nullable Comparator<TaskDTO> comparator);

    List<TaskDTO> findAllForUser(@NotNull String userId, @Nullable Comparator<TaskDTO> comparator);

    TaskDTO findOneByIdForUser(@NotNull String userId, @NotNull String id);

    @Nullable TaskDTO findOneById(@NotNull String id);

    TaskDTO findOneByIndexForUser(@NotNull String userId, @NotNull Integer index);

    TaskDTO findOneByIndex(@NotNull Integer index);

    int getSize();

    int getSizeForUser(@NotNull String userId);

    void removeOneByIdForUser(@NotNull String userId, @NotNull String id);

    void removeOneById(@NotNull String id);

    boolean existsByIdForUser(@NotNull String userId, @NotNull String id);

    boolean existById(@NotNull String id);

    List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId);
}
