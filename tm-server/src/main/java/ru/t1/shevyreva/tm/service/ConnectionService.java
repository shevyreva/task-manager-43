package ru.t1.shevyreva.tm.service;

import lombok.SneakyThrows;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.IDatabaseProperty;
import ru.t1.shevyreva.tm.dto.model.ProjectDTO;
import ru.t1.shevyreva.tm.dto.model.SessionDTO;
import ru.t1.shevyreva.tm.dto.model.TaskDTO;
import ru.t1.shevyreva.tm.dto.model.UserDTO;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.model.Session;
import ru.t1.shevyreva.tm.model.Task;
import ru.t1.shevyreva.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final EntityManagerFactory entityManagerFactory;
    @NotNull IDatabaseProperty databaseProperty;

    public ConnectionService(@NotNull IDatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
        this.entityManagerFactory = factory();
    }

    @NotNull
    @Override
    @SneakyThrows
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    public EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, databaseProperty.getDatabaseDriver());
        settings.put(org.hibernate.cfg.Environment.URL, databaseProperty.getDatabaseUrl());
        settings.put(org.hibernate.cfg.Environment.USER, databaseProperty.getDatabaseUsername());
        settings.put(org.hibernate.cfg.Environment.PASS, databaseProperty.getDatabasePassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, databaseProperty.getDatabaseDialect());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, databaseProperty.getHBM2DDL());
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, databaseProperty.getShowSQL());

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(SessionDTO.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}
