package ru.t1.shevyreva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.model.ISessionRepository;
import ru.t1.shevyreva.tm.model.Session;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionRepository extends AbstractUserOwnedModelRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clearForUser(@NotNull String userId) {
        entityManager.createQuery("DELETE FROM Session e WHERE e.userId  = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeOneByIdForUser(@NotNull String userId, @NotNull String id) {
        entityManager.remove(findOneByIdForUser(userId, id));
    }

    @Override
    public List<Session> findAllForUser(@NotNull String userId) {
        return entityManager.createQuery("SELECT e FROM Session e WHERE e.userId  = :userId", Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public Session findOneByIdForUser(@NotNull String userId, @NotNull String id) {
        return entityManager.createQuery("SELECT e FROM Session e WHERE e.id  = :id AND e.userId  = :userId", Session.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public int getSizeForUser(@NotNull String userId) {
        return entityManager.createQuery("SELECT COUNT(e) FROM Session e WHERE e.userId  = :userId", Integer.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM Session")
                .executeUpdate();
    }

    @Override
    public @Nullable List<Session> findAll() {
        return entityManager.createQuery("SELECT e FROM Session e", Session.class).getResultList();
    }

    @Override
    public @Nullable Session findOneById(@NotNull String id) {
        return entityManager.find(Session.class, id);
    }

    @Override
    public void removeOneById(@NotNull String id) {
        entityManager.remove(findOneById(id));
    }

    @Override
    public void removeOne(@NotNull final Session session) {
        entityManager.remove(session);
    }


    @Override
    public int getSize() {
        return entityManager.createQuery("SELECT COUNT(e) FROM Session e", Integer.class)
                .setMaxResults(1)
                .getSingleResult();
    }

}
