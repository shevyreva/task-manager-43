package ru.t1.shevyreva.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.List;

public interface IAbstractUserOwnedModelDtoRepository<M extends AbstractUserOwnedModelDTO> extends IAbstractModelDtoRepository<M> {


    void addForUser(@NotNull final String userId, @NotNull final M model);

    void updateForUser(@NotNull final String userId, @NotNull final M model);

    abstract void clearForUser(@NotNull final String userId);

    abstract void removeOneByIdForUser(@NotNull final String userId, @NotNull final String id);

    abstract List<M> findAllForUser(@NotNull final String userId);

    abstract M findOneByIdForUser(@NotNull final String userId, @NotNull final String id);

    abstract int getSizeForUser(@NotNull final String userId);

}


